fn main() {
    println!("--- Day One! ---");

    let elves = get_list();
    first(&elves);
    second(&elves);
}

fn get_list() -> Vec<usize> {
    let input = include_str!("../input");

    // Get a Vec over the lines (iters dont have the Split method)
    let elves = input.lines().collect::<Vec<&str>>();

    // Get a seperate iter over _each_ elf
    let elves = elves.split(|x| x == &"");

    // Take the sum of each elf
    let mut elves = elves
        .map(|x| x.iter().map(|x| x.parse::<usize>().unwrap()).sum())
        .collect::<Vec<usize>>();

    elves.sort();
    elves.reverse();
    elves
}

fn first(elves: &[usize]) -> usize {
    let ans = elves[1];
    println!("--- Day One Answer, Part One: ---\n{}", ans);

    ans
}

fn second(elves: &[usize]) -> usize {
    let total = <&[usize] as TryInto<&[usize]>>::try_into(&elves[0..=2])
        .unwrap()
        .iter()
        .sum();

    println!("--- Day One Answer, Part Two: ---\n{:?}", total);
    total
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn one() {
        assert_eq!(first(&get_list()), 71575);
    }

    #[test]
    fn two() {
        assert_eq!(second(&get_list()), 213958);
    }
}
