#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
enum Rps {
    Rock,
    Paper,
    Scissors,
}
use Rps::*;

const INPUT: &str = include_str!("../input");

struct Parser {
    is: Rps,
    beats: Rps,
    chars: [char; 2],
    score: usize,
}

enum State {
    Win,
    Lose,
    Draw,
}
impl From<char> for State {
    fn from(inp: char) -> Self {
        match inp {
            'X' => Self::Lose,
            'Y' => Self::Draw,
            'Z' => Self::Win,
            _ => unreachable!(),
        }
    }
}

const MOVES: [Parser; 3] = [
    Parser {
        is: Rock,
        beats: Scissors,
        chars: ['A', 'X'],
        score: 1,
    },
    Parser {
        is: Paper,
        beats: Rock,
        chars: ['B', 'Y'],
        score: 2,
    },
    Parser {
        is: Scissors,
        beats: Paper,
        chars: ['C', 'Z'],
        score: 3,
    },
];

fn find(inp: &str) -> &Parser {
    MOVES
        .iter()
        .find(|item| item.chars.contains(&inp.chars().nth(0).unwrap()))
        .unwrap()
}

fn main() {
    one();
    two();
}

fn one() -> usize {
    let out: usize = INPUT
        .lines()
        .map(|x| {
            let mut x = x.split(' ');

            let other = find(x.next().unwrap());
            let own = find(x.next().unwrap());

            let mut score = if other.beats == own.is {
                0
            } else if other.is == own.is {
                3
            } else if own.beats == other.is {
                6
            } else {
                panic!()
            };

            score += own.score;

            score
        })
        .sum();

    println!("One: {out}");
    out
}

fn two() -> usize {
    let ans: usize = INPUT
        .lines()
        .map(|x| {
            let mut x = x.split(' ');

            let other = find(x.next().unwrap());
            let to_play: State = x.next().unwrap().chars().nth(0).unwrap().into();

            let (own, mut total) = match to_play {
                State::Win => (MOVES.iter().find(|item| item.beats == other.is).unwrap(), 6),
                State::Lose => (MOVES.iter().find(|item| other.beats == item.is).unwrap(), 0),
                State::Draw => (MOVES.iter().find(|item| other.is == item.is).unwrap(), 3),
            };

            total += own.score;

            total
        })
        .sum();

    println!("Two: {ans}");
    ans
}

#[cfg(test)]
mod tests {
    #[test]
    fn one() {
        assert_eq!(super::one(), 15572);
    }

    #[test]
    fn two() {
        assert_eq!(super::two(), 16098);
    }
}
