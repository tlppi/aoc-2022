const INPUT: &str = include_str!("../input");

fn main() {
    let [part1, part2] = exec();

    println!("Part One: {}", part1);
    println!("Part Two: {}", part2);
}

fn exec() -> [usize; 2] {
    let nums = INPUT.lines().map(|x| {
        // X is `a-b,c-d`
        let pairs: Vec<[u8; 2]> = x
            .split(',')
            .map(|y| {
                // Y is `a-b`
                let nums: Vec<u8> = y.split('-').map(|z| z.parse::<u8>().unwrap()).collect();
                [nums[0], nums[1]]
            })
            .collect();
        [pairs[0], pairs[1]]
    });

    let part1 = nums
        .clone()
        .filter(|i| {
            let [elf1, elf2] = i;

            (elf1[0] >= elf2[0] && elf1[1] <= elf2[1]) || (elf2[0] >= elf1[0] && elf2[1] <= elf1[1])
        })
        .count();

    let part2 = nums
        .filter(|i| {
            let [elf1, elf2] = i;

            (elf1[0] <= elf2[0] && elf1[1] >= elf2[0]) || (elf1[0] >= elf2[0] && elf1[0] <= elf2[1])
        })
        .count();


    [part1, part2]
}

#[cfg(test)]
mod tests {
    #[test]
    fn one() {
        assert_eq!(super::exec()[0], 511);
    }

    #[test]
    fn two() {
        assert_eq!(super::exec()[1], 821);
    }
}