const INPUT: &str = include_str!("../input");

#[derive(Debug)]
struct Backpack {
    compart1: Vec<char>,
    compart2: Vec<char>,
}

impl From<&str> for Backpack {
    fn from(x: &str) -> Self {
        let chars: Vec<char> = x.chars().collect();
        let mut chars = chars.chunks(x.len() / 2);

        Backpack {
            compart1: chars.next().unwrap().to_owned(),
            compart2: chars.next().unwrap().to_owned(),
        }
    }
}

impl Backpack {
    fn find_match(&self) -> char {
        for i in &self.compart1 {
            if self.compart2.contains(i) {
                return *i;
            }
        }

        unreachable!();
    }
}

fn get_priority(c: char) -> usize {
    if c.is_lowercase() {
        return c as usize - 96;
    } else {
        return c as usize - 38;
    }
}

fn one() -> usize {
    let out: usize = INPUT
        .lines()
        .map(|x| {
            let b: Backpack = x.into();
            get_priority(b.find_match())
        })
        .sum();

    println!("{out}");
    out
}

fn two() -> usize {
    let l: Vec<&str> = INPUT.lines().collect();
    let out: usize = l.chunks(3).map(|x| {
        for i in x[0].chars() {
            if x[1].chars().collect::<Vec<char>>().contains(&i)
                && x[2].chars().collect::<Vec<char>>().contains(&i)
            {
                return get_priority(i);
            }
        }

        unreachable!()
    }).sum();

    println!("{out}");
    out
}

fn main() {
    one();
    two();
}

#[cfg(test)]
mod tests {
    #[test]
    fn one() {
        assert_eq!(super::one(), 8088)
    }

    #[test]
    fn two() {
        assert_eq!(super::two(), 2522)
    }
}