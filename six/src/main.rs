use std::collections::VecDeque;

const INPUT: &str = include_str!("../input");


fn main() {
    println!("One: {}\nTwo: {}", one(), two());
}

fn one() -> usize {
    let mut chars = INPUT.chars().into_iter();

    let mut queue = VecDeque::new();
    for i in 1..=INPUT.len() {
        queue.push_back(chars.next().unwrap());

        if queue.len() < 4 { continue }

        let mut v: Vec<_> =  queue.clone().into();
        v.sort();
        v.dedup();
        if v.len() == 4 {
            return i;
        }

        queue.pop_front();
    }

    unreachable!();
}


fn two() -> usize {
    let mut chars = INPUT.chars().into_iter();

    let mut queue = VecDeque::new();
    for i in 1..=INPUT.len() {
        queue.push_back(chars.next().unwrap());

        if queue.len() < 14 { continue }

        let mut v: Vec<_> =  queue.clone().into();
        v.sort();
        v.dedup();
        if v.len() == 14 {
            return i;
        }

        queue.pop_front();
    }

    unreachable!();
}
