use std::collections::VecDeque;

const INPUT: &str = include_str!("../input");

fn main() {
    let [one, two] = calc();
    println!("One: {}\nTwo: {}", one, two);
}

fn calc() -> [String; 2] {
    let inp = INPUT.split("\n\n").collect::<Vec<&str>>();
    let (diagram, instructions) = (inp[0], inp[1]);

    let num_piles = diagram.lines().nth(0).unwrap().len() / 4 + 1;

    // Parse the STACKS
    let mut diagram = diagram.lines().collect::<Vec<&str>>();
    diagram.pop();
    let diagram = diagram.into_iter();

    let things: Vec<Vec<char>> = diagram
        .map(|x| {
            let mut chars: Vec<char> = x.chars().collect();
            chars.push(' ');

            chars
                .chunks(4)
                .map(|y| y.to_owned()[1])
                .collect::<Vec<char>>()
        })
        .collect();

    let stacks: Vec<Vec<char>> = (0..num_piles)
        .map(|i| {
            let mut x = Vec::new();
            for j in &things {
                x.push(j[i])
            }

            x = x.into_iter().rev().filter(|x| x != &' ').collect();
            x
        })
        .collect();

    // Parse INSTRUCTIONS
    let ins: Vec<[usize; 3]> = instructions
        .lines()
        .map(|x| {
            let toks: Vec<&str> = x.split(' ').collect();
            let toks = [toks[1], toks[3], toks[5]];
            let toks: Vec<usize> = toks
                .into_iter()
                .map(|s| s.parse::<usize>().unwrap())
                .collect();

            toks.try_into().unwrap()
        })
        .collect();

    let one = one(&ins, &stacks);
    let two = two(&ins, &stacks);


    [one, two]
}

fn one(ins: &Vec<[usize; 3]>, stacks: &Vec<Vec<char>>) -> String {
    let mut stacks = stacks.clone();

    for [num, from, to] in ins {
        for _ in 0..*num {
            let item = stacks[from - 1].pop().unwrap();
            stacks[to - 1].push(item);
        }
    }

    let total = stacks.into_iter().map(|x| x.last().unwrap().clone());

    let mut out = String::new();
    total.for_each(|f| out += &*(f.to_string()));

    out
}

fn two(ins: &Vec<[usize; 3]>, stacks: &Vec<Vec<char>>) -> String {
    let mut stacks = stacks.clone();

    for [num, from, to] in ins {
        let mut y = VecDeque::new();
        for _ in 0..*num {
            let item = stacks[from - 1].pop().unwrap();
            y.push_front(item);
        }
        stacks[to - 1].append(&mut y.into());
    }

    let total = stacks.into_iter().map(|x| x.last().unwrap().clone());

    let mut out = String::new();
    total.for_each(|f| out += &*(f.to_string()));

    out
}

#[cfg(test)]
mod tests {
    #[test] 
    fn one() {
        assert_eq!(super::calc()[0], "QPJPLMNNR".to_string())
    }

    #[test]
    fn two() {
        assert_eq!(super::calc()[1], "BQDNWJPVJ".to_string())
    }
}